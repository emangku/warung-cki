import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import StackNavigaton from './navigation/StackNavigation'

import * as SQLite from 'expo-sqlite'
const db = SQLite.openDatabase("example.db");

const Stack = createStackNavigator();
export default function App() {
  React.useEffect(()=>{
    db.transaction(tx => {
      tx.executeSql("create table if not exists products (id integer primary key not null, image string, name text, price string);")
    })
  },[]);
  
  return (
    <View style={styles.container}>
      <StatusBar style="auto" />
      <NavigationContainer>
          <Stack.Navigator 
            screenOptions={{title:'', headerTransparent:true }}>
            <Stack.Screen name="Root" component={StackNavigaton}></Stack.Screen>
          </Stack.Navigator>
      </NavigationContainer>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
});
