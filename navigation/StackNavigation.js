import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Login from '../components/Login'
import Home from '../components/Home'
import FormsInput from '../components/FormInput'
const INITIAL_ROUTE_NAME = 'Login';
const Stack = createStackNavigator()
export default function StackNavigation({navigation, route}){
    return (
        <Stack.Navigator initialRouteName={INITIAL_ROUTE_NAME}>
            <Stack.Screen name="Login" component={Login}></Stack.Screen>
            <Stack.Screen name="Home" component={Home}></Stack.Screen>
            <Stack.Screen 
                name="Form" 
                component={FormsInput}></Stack.Screen>
        </Stack.Navigator>
    )
}