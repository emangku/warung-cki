import React from 'react';
import { 
    KeyboardAvoidingView,
    Platform,
    StyleSheet, 
    Text, 
    View, 
    TextInput, 
    TouchableOpacity 
} from 'react-native';


export default class Login extends React.Component{
    constructor(props){
        super(props)
        this.state={
            email:"",
            password:""
        }
    }
    
   
    render(){
        
    
        return (
          <View style={styles.container}>
            <KeyboardAvoidingView style={{
                width:'100%',
                height:'100%',
                justifyContent:'center',
                alignItems:'center'
            }} 
            behavior={Platform.OS == "ios" ? "padding" : "height"}>
            <Text style={styles.logo}>Warung Cki</Text>
            <View style={styles.inputView} >
              <TextInput  
                style={styles.inputText}
                placeholder="Email..." 
                placeholderTextColor="#003f5c"
                onChangeText={text => this.setState({email:text})}/>
            </View>
            <View style={styles.inputView} >
              <TextInput  
                secureTextEntry
                style={styles.inputText}
                placeholder="Password..." 
                placeholderTextColor="#003f5c"
                onChangeText={text => this.setState({password:text})}/>
            </View>
            <TouchableOpacity>
              <Text style={styles.forgot}>Forgot Password?</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.loginBtn}
                onPress={ () => this.props.navigation.navigate('Root',{screen:'Home'})  }
            >
              <Text style={styles.loginText}>LOGIN</Text>
            </TouchableOpacity>
            <TouchableOpacity>
              <Text style={styles.loginText}>Signup</Text>
            </TouchableOpacity>
    
            </KeyboardAvoidingView>
          </View>
        );
      }
      
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    logo:{
      fontWeight:"bold",
      fontSize:50,
      color:"#0000FF",
      marginBottom:40
    },
    inputView:{
      width:"80%",
      backgroundColor:"#465881",
      borderRadius:25,
      height:50,
      marginBottom:20,
      justifyContent:"center",
      padding:20
    },
    inputText:{
      height:50,
      color:"white"
    },
    forgot:{
      color:"white",
      fontSize:11
    },
    loginBtn:{
      width:"80%",
      backgroundColor:"#0000FF",
      borderRadius:25,
      height:50,
      alignItems:"center",
      justifyContent:"center",
      marginTop:40,
      marginBottom:10
    },
    loginText:{
      color:"white"
    }
  });
//()=>props.onPress?.()
//   <Image source={require('./my-icon.png')} />