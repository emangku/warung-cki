import * as React from 'react'
import { Card } from 'react-native-elements';
import { TouchableOpacity, View, Text, StyleSheet } from 'react-native';


const CardItem = (props) =>{
    return (
        <>
            <TouchableOpacity onPress={()=> props.onDetail?.()}>
                <Card 
                    image={{uri: props.image}} 
                    imageStyle={{
                        resizeMode:"stretch",
                        padding:10,
                        height:100
                    }}
                    containerStyle={{
                        width: 150,
                    }}>
                    <View>
                        <Text>{props.price}</Text>
                    </View>
                    <View 
                        style={{
                            flexDirection:'row-reverse', 
                            justifyContent:'space-around', 
                            alignItems:'center',
                            paddingTop:15,
                            paddingHorizontal:5}}>
                        <TouchableOpacity 
                            style={styles.btnDetails}
                            onPress={() => props.onDelete?.()}>
                            <Text style={styles.textDetail}>delete</Text>
                        </TouchableOpacity>
                        <TouchableOpacity 
                            style={styles.btnDetails}
                            onPress={()=> props.onUpdate?.()}>
                            <Text style={styles.textDetail}>Update</Text>
                        </TouchableOpacity>
                    </View>
                </Card>
            </TouchableOpacity>
        </>
    )
}

export default CardItem;

const styles = StyleSheet.create({
    btnDetails:{
        height: 20,
        width:'45%',
        borderWidth:1,
        justifyContent:'center',
        alignItems:'center',
        borderColor:'#000',
        paddingHorizontal:10,
    },
    textDetail:{
        fontSize: 8,
    }
})