import * as React from 'react'
import { View , Text, TextInput, StyleSheet} from 'react-native'

import * as SQLite from 'expo-sqlite'
import { TouchableOpacity } from 'react-native'
const db = SQLite.openDatabase("warkop_cki.db");
export default function FormInput(props){
    const [image, setImage] = React.useState('')
    const [name, setName] = React.useState('')
    const [price, setPrice] = React.useState('')

    const add = () =>{
        db.transaction(
            tx => {
                tx.executeSql(
                    'insert into products (image, name, price) values(?,?,?)',[
                        image,
                        name,
                        price
                    ],
                    () => {
                        props.navigation.goBack()
                    }
                )
            }
        )
    }

    return(
        <>
            <View style={styles.container}>
                <Text>{price}</Text>
                <TextInput 
                    placeholder="Image URL" 
                    style={styles.input}
                    onChangeText={text => setImage(text)}  />
                <TextInput 
                    placeholder="Name" 
                    style={styles.input}
                    onChangeText={text => setName(text)}  />
                <TextInput 
                    onChangeText={text => setPrice(text)}
                    placeholder="Price" 
                    keyboardType="number-pad"
                    style={styles.input}  />
                <TouchableOpacity 
                    onPress={()=> add() }
                    style={styles.btn}>
                    <Text style={styles.btnText}>Submit</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.btnBack} onPress={()=>{ props.navigation.goBack() }}>
                    <Text style={styles.btnText}>Back</Text>
                </TouchableOpacity>
            </View>
        </>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        flexDirection:'column',
        marginTop:30,
        padding: 20,
    },
    input:{
        width:'100%',
        height:40,
        borderWidth:1,
        paddingLeft:20,
        paddingRight:10,
        borderRadius:5,
        marginTop:10
    },
    btn:{
        width:'100%',
        height:40,
        borderWidth:1,
        marginTop:20,
        justifyContent:'center',
        alignItems:'center'
    },
    btnText:{
        color:'#000'
    },
    btnBack:{
        width:'100%',
        height:40,
        marginTop:20,
        justifyContent:'center',
        alignItems:'center'
    }

})