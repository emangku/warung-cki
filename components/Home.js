//@ts-nocheck
import React from 'react';
import { 
    KeyboardAvoidingView,
    Platform,
    StyleSheet, 
    Alert,
    Text, 
    View, 
    TextInput, 
    TouchableOpacity,
    FlatList,
    Modal
} from 'react-native';
import CardItem from './widgets/Card';
import { FloatingAction } from "react-native-floating-action";

import * as SQLite from 'expo-sqlite'

const db = SQLite.openDatabase("warkop_cki.db");
export default function Home(props){
    const [modalVisible, setModalVisible] = React.useState(false)
    const [items, setItems] = React.useState([])
    const [image, setImage] = React.useState('');
    const [name, setName] = React.useState('');
    const [price, setPrice] = React.useState(0);
    const [id, setId] = React.useState(0);
    const [data, setData] = React.useState({});

        const toForm =() =>{
            props.navigation.navigate('Root',{ screen: 'Form' })
        }
        const getAllData =()=>{
            db.transaction(
                tx => {
                    tx.executeSql(
                        'select * from  products',
                        [],
                        (_,{ rows })=>{
                            setItems(rows._array)
                        }
                    )
            })
        }
        const Delete = (id,name) => {
            db.transaction(
                tx => {
                    tx.executeSql("delete from products where id=?",[id],()=>{
                        Alert.alert("Successs delete "+name)
                        getAllData()
                    })
                }
            )
        } 

        const showModal = (item) => {
            setData(item)
            setModalVisible(true)
        }

        React.useEffect(()=>{
            getAllData()
        },[])
        React.useEffect(()=>{
            getAllData()
        },[])
        
        return(
            <>

                <View style={{
                        flex:1,
                        justifyContent:'center', 
                        alignItems:'center', 
                        marginTop:30
                }}>
                    <TouchableOpacity 
                        style={styles.btnRefresh}
                    onPress={() => getAllData() }>
                        <Text> Refresh </Text>
                    </TouchableOpacity>
                    <FlatList
                        data={items}
                        renderItem={({item})=>(
                            <CardItem
                                onDelete={()=> Delete(item.id, items.name)}
                                onUpdate={() => {
                                    // showModal(item.id,item.image, item.desc, item.price);
                                    showModal(item);
                                }}
                                image={item.image} />
                        )}
                        numColumns={2}>

                        </FlatList>
                    <FloatingAction 
                        showBackground={false} 
                        onPressMain={() => toForm()  }> </FloatingAction>
                    <Modal 
                        animationType="slide"
                        visible={modalVisible}
                        transparent={true}
                        onRequestClose={() => {
                          Alert.alert("Modal has been closed.");
                        }}>
                            <View style={styles.centeredView}>
                            <View style={styles.modalView}>
                                <View style={{marginBottom: 10}}>
                                    <TextInput 
                                        onChangeText={text => setImage(text)}
                                        value={data.image}
                                     placeholder="Input Urle"
                                    style={{
                                        width:250,
                                        borderWidth:1,
                                        height: 40,
                                        paddingHorizontal:3,
                                        marginBottom:5,
                                    }} />
                                    <TextInput 
                                    onChangeText={text => setPrice(text)}
                                    value={data.price}
                                    placeholder="Price"
                                    keyboardType="number-pad"
                                    style={{
                                        width:250,
                                        borderWidth:1,
                                        height: 40,
                                        marginBottom:5,
                                        paddingHorizontal:3
                                    }} />
                                    <TextInput 
                                        onChangeText={text => setName(text)}
                                        value={data.name}
                                    placeholder="Desc"
                                    multiline={true}
                                    style={{
                                        width:200,
                                        borderWidth:1,
                                        height: 40,
                                        paddingHorizontal:3
                                    }} />
                                </View>
                                <View style={{
                                    flexDirection:'row-reverse',
                                    justifyContent:'flex-start'
                                }}>
                                <TouchableOpacity
                                    style={{ ...styles.openButton, backgroundColor: "#2196F3" }}
                                    onPress={() => {
                                        setModalVisible(!modalVisible);
                                    }}
                                    >
                                    <Text style={styles.textStyle}>Update</Text>
                                </TouchableOpacity>

                                <TouchableOpacity><Text> </Text></TouchableOpacity>
                                <TouchableOpacity
                                    style={{ ...styles.openButton, backgroundColor: "#2196F3" }}
                                    onPress={() => {
                                        setModalVisible(!modalVisible);
                                    }}
                                    >
                                    <Text style={styles.textStyle}>Cancel</Text>
                                </TouchableOpacity>
                                </View>
                            </View>
                            </View>

                    </Modal>
                </View>
            </>
        )
}

const styles = StyleSheet.create({
    centeredView: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
      marginTop: 10
    },
    modalView: {
      margin: 20,
      backgroundColor: "white",
      borderRadius: 5,
      padding: 40,
      alignItems: "center",
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 2
      },
      shadowOpacity: 0.25,
      shadowRadius: 3.84,
      elevation: 5
    },
    openButton: {
      backgroundColor: "#F194FF",
      borderRadius: 20,
      padding: 5,
      elevation: 2,
      paddingHorizontal:10
    },
    textStyle: {
      color: "white",
      fontWeight: "bold",
      textAlign: "center",
      marginVertical:5,
      marginHorizontal:5
    },
    modalText: {
      marginBottom: 15,
      textAlign: "center"
    },
    btnRefresh:{
        justifyContent:'center',
        height:40,
        alignItems:'center',
        width:'95%',
        borderWidth:1,
        paddingLeft:20,
        paddingRight:20
    }
  });

